#include <pthread.h>
#include <sys/sysinfo.h>

#include <iostream>
using std::cerr;
using std::endl;

#include "api.hpp"
#include "fcgi_config.h"
#include "fcgiapp.h"
#include "router.hpp"
#include "jwt_scheme.hpp"
#include "interface_db.h"

#define STDIN_MAX 1000000

#define DEFAULT_SOCKET_PATH "127.0.0.1:8000"

enum statusCode {
    OK = 0,
    ERROR_LIB_INIT = 1,
    ERROR_SOCKET_OPEN = 2,
    ERROR_THREAD_CREATE = 3,
    ERROR_REQUEST_INIT = 4,
    ERROR_REQUEST_ACCEPT = 5,
    ERROR_REQUEST_BODY = 6
};

string getRequestContent(FCGX_Request *request) {
    char *contentLengthString = FCGX_GetParam("CONTENT_LENGTH", request->envp);
    if (!contentLengthString) {
        cerr << ERROR_REQUEST_BODY << endl;
        return "{}";
    }
    unsigned long contentLength = STDIN_MAX;

    if (contentLengthString) {
        contentLength = strtol(contentLengthString, &contentLengthString, 10);

        if (contentLength > STDIN_MAX) {
            contentLength = STDIN_MAX;
        }
    } else {
        cerr << ERROR_REQUEST_BODY << endl;
        return "{}";
    }

    char *contentBuffer = new char[contentLength + 1];
    FCGX_GetStr(contentBuffer, contentLength, request->in);

    string content = contentBuffer;
    delete contentBuffer;

    return content;
}

map<string, string> getRequestHeaders(FCGX_Request *request) {
    map<string, string> headers;

    const char *header = nullptr;
    if (header = FCGX_GetParam("AUTHORIZATION", request->envp)) {
        headers["AUTHORIZATION"] = header;
    } else {
        headers["AUTHORIZATION"] = "";
    }

    if (header = FCGX_GetParam("CONTENT_TYPE", request->envp)) {
        headers["CONTENT_TYPE"] = header;
    } else {
        headers["CONTENT_TYPE"] = "";
    }

    /* Если нужны еще заголовки, добавлять ниже. Добавлять все полученные сразу - лишняя нагрузка */

    return headers;
}

void returnResponse(const HTTP::Response &response, FCGX_Stream *outStream) {
    /* Запись заголовков */
    for (auto header : response.headers) {
        string line = header.first + ": " + header.second + "\r\n";
        FCGX_PutS(line.c_str(), outStream);
    }

    /* Запись тела ответа */
    FCGX_PutS("\r\n", outStream);

    FCGX_PutS((response.body.toStyledString() + "\r\n").c_str(), outStream);

    /* Код ответа */
    FCGX_SetExitStatus((int)response.statusCode, outStream);
}

static void *handlerRoutine(void *socketId) {
    statusCode result = OK;
    FCGX_Request *fcgiRequest = new FCGX_Request;

    if (FCGX_InitRequest(fcgiRequest, *((int *)socketId), 0)) {
        delete fcgiRequest;
        cerr << result << endl;
        return nullptr;
    }

    while (true) {
        static pthread_mutex_t acceptMutex = PTHREAD_MUTEX_INITIALIZER;

        pthread_mutex_lock(&acceptMutex);
        int acceptable = FCGX_Accept_r(fcgiRequest);
        pthread_mutex_unlock(&acceptMutex);

        if (acceptable < 0) {
            cerr << ERROR_REQUEST_ACCEPT << endl;
            break;
        }

        string body = getRequestContent(fcgiRequest);
        map<string, string> headers = getRequestHeaders(fcgiRequest);
        HTTP::Request request(string(FCGX_GetParam("REQUEST_METHOD", fcgiRequest->envp)),
                              string(FCGX_GetParam("DOCUMENT_URI", fcgiRequest->envp)),
                              string(FCGX_GetParam("QUERY_STRING", fcgiRequest->envp)), headers, body);

        /* Боевая реализация */
        authenticate(request);
        Router router(request);
        HTTP::Response response = router.Execute();

        /* Пример эхо-сервера */
        // HTTP::Response response(HTTP::ResponseCode::OK, request.headers, request.body);
        returnResponse(response, fcgiRequest->out);

        FCGX_Finish_r(fcgiRequest);
    }

    delete fcgiRequest;

    return nullptr;
}

/* Принимает единственный аргумент - host:port, где нужно открыть сокет */
int main(int argc, char **argv) {
    initDB();

    statusCode result = OK;

    int threadCount = get_nprocs();
    pthread_t *handlers = new pthread_t[threadCount];

    if (FCGX_Init()) {
        delete handlers;
        return ERROR_LIB_INIT;
    }

    /* Открываем сокет с размером очереди в 2 раза больше количества рабочих потоков */
    int socketId = -1;
    if (argc == 2) {
        socketId = FCGX_OpenSocket(argv[1], 2 * threadCount);
    } else {
        socketId = FCGX_OpenSocket(DEFAULT_SOCKET_PATH, 2 * threadCount);
    }

    if (socketId < 0) {
        delete handlers;
        return ERROR_SOCKET_OPEN;
    }

    /* Создаём рабочие потоки */
    int *sockets = new int[threadCount];
    for (int i = 0; i < threadCount; i++) {
        sockets[i] = socketId;
        if (pthread_create(handlers + i, nullptr, handlerRoutine, sockets + i)) {
            for (int j = 0; j < i; j++) {
                pthread_join(handlers[i], nullptr);
            }
            delete handlers;
            delete sockets;
            return ERROR_THREAD_CREATE;
        }
    }

    /* Ждем завершения рабочих потоков */
    for (int i = 0; i < threadCount; i++) {
        pthread_join(handlers[i], nullptr);
    }

    delete handlers;
    delete sockets;

    return result;
}
