#include "record.hpp"

Record::Record(const Json::Value& data)
{
    id = data["id"].asInt();
    description = data["description"].asString();
    reportStatus = data["reportStatus"].asBool();
    idUser = data["idUser"].asInt();
    idGoal = data["idGoal"].asInt();
    // fielId = data["fielId"].asString();
}

Json::Value Record::ToJson()
{
    Json::Value user;
    user["id"] = id;
    user["description"] = description;
    user["reportStatus"] = reportStatus;
    user["idUser"] = idUser;
    user["idGoal"] = idGoal;
    // user["fielId"] = fielId;
    return user;
}

void Record::FromJson(const Json::Value &data)
{
    id = data["id"].asInt();
    description = data["description"].asString();
    reportStatus = data["reportStatus"].asBool();
    idUser = data["idUser"].asInt();
    idGoal = data["idGoal"].asInt();
    // fielId = data["fielId"].asString();
}

void Record::setGoalId(int id) {
    idGoal = id;
}

void Record::setUserId(int id) {
    idUser = id;
}