#include "goal.hpp"
#include <algorithm>

Goal::Goal(const Json::Value& data):
    id(data["id"].asInt()),
    title(data["title"].asString()), 
    description(data["description"].asString()){}


void Goal::SetUserId(int id) {
    userId = id;
}

void Goal::SetUserCreatorId(int id) {
    creatorUserId =id;
}

Json::Value Goal::ToJson() {
    Json::Value goal;
    goal["id"] = id;
    goal["creatorUserId"] = creatorUserId;
    goal["title"] = title;
    goal["description"] = description;
    goal["userId"] = userId;
    return goal;
}

void Goal::FromJson(const Json::Value& data) {
    id = data["id"].asInt();
    creatorUserId = data["creatorUserId"].asInt();
    title = data["title"].asString();
    description = data["description"].asString();
    userId = data["userId"].asInt();

}
