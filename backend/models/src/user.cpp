#include "user.hpp"

Json::Value User::ToJson() {
    Json::Value user;
    user["id"] = id;
    user["email"] = email;
    user["login"] = login;
    user["password"] = password;
    user["firstName"] = firstName;
    user["lastName"] = lastName;
    return user;
}

void User::FromJson(const Json::Value& data) {
    id = data["id"].asInt();
    email = data["email"].asString();
    login = data["login"].asString();
    password = data["password"].asString();
    firstName = data["firstName"].asString();
    lastName = data["lastName"].asString();
}
