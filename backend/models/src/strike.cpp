#include "strike.hpp"

Json::Value Strike::ToJson()
{
    Json::Value strike;
    strike["id"] = id;
    strike["fromId"] = fromId;
    strike["recordId"] = recordId;
    strike["status"] = status;
    strike["description"] = description;
    return strike;
}

void Strike::FromJson(const Json::Value &data)
{
    id = data["id"].asInt();
    fromId = data["fromId"].asInt();
    status = data["status"].asString();
    recordId = data["recordId"].asInt();
    description = data["description"].asString();
}

void Strike::SetFromId(int id)
{
    fromId = id;
}

