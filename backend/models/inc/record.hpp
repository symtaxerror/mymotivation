#pragma once

#include "base_model.hpp"

#include "base_model.hpp"
using std::string;
using std::vector;

class Record : public BaseModel {
   private:
    int id;
    string description;
    bool reportStatus;
    int idUser;
    int idGoal;
    // string fileId;

   public:
    const static inline vector<string> fields = { "id", "description", "user_id", "goal_id", "file_id" };

   public:
    Record() {}
    Record(int _id):id(_id) {}
    void setGoalId(int id);
    void setUserId(int id);
    Record(const Json::Value& data);
    void FromJson(const Json::Value& data) override;
    Json::Value ToJson() override;
};
