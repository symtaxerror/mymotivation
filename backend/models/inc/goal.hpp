#pragma once

#include "base_model.hpp"
using std::string;
using std::vector;

class Goal : public BaseModel {
   private:
    int id;
    int creatorUserId;
    string title;
    string description;
    int userId;

   public:
    const static inline vector<string> fields = { "id", "userId", "title", "description", "creatorUserId"};

   public:
    Goal() {}
    Goal(int _id): id(_id){}
    Goal(const Json::Value& data);
    void FromJson(const Json::Value& data) override;
    Json::Value ToJson() override;
    void SetUserId(int id);
    void SetUserCreatorId(int id);
};
