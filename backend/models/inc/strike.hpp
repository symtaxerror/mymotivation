#pragma once

#include "base_model.hpp"
using std::string;
using std::vector;

class Strike : public BaseModel {
   private:
    int id;
    int fromId;
    int recordId;
    string status;
    string description;

   public:
    const static inline vector<string> fields = {
        "id", "fromId", "recordId", "status","description"
    };

   public:
    Strike() {}
    Strike(const Json::Value& data)
        : id(data["id"].asInt()),
          fromId(data["fromId"].asInt()),
          recordId(data["recordId"].asInt()),
          status(data["status"].asString()),
          description(data["description"].asString()){}
    void FromJson(const Json::Value& data) override;
    Json::Value ToJson() override;
    void SetFromId(int id);
};