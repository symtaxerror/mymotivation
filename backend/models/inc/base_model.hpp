#pragma once

#include <jsoncpp/json/json.h>

enum Model{
    USER,
    GOAL,
    STRIKE,
    RECORD,
    MODERATOR,
    SESSION
};

class BaseModel {
   public:
    virtual ~BaseModel() {}

   public:
    virtual void FromJson(const Json::Value& data) = 0;
    virtual Json::Value ToJson() = 0;
};
