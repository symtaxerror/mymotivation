#pragma once

#include <string>
#include <vector>

#include "base_model.hpp"
using std::string;
using std::vector;

class User : public BaseModel {
   private:
    int id;
    string email;
    string login;
    string password;
    string firstName;
    string lastName;

   public:
    const static inline vector<string> fields = {
        "id", "email", "login", "password", "firstName", "lastName"
    };

   public:
    User() {}
    User(int _id):id(_id){}
    User(const Json::Value& data)
        : id(data["id"].asInt()),
          email(data["email"].asString()),
          login(data["login"].asString()),
          password(data["password"].asString()),
          firstName(data["firstName"].asString()),
          lastName(data["lastName"].asString()) {}
    void FromJson(const Json::Value& data) override;
    Json::Value ToJson() override;
};
