#include "parsers.hpp"

#include <algorithm>
#include <cctype>

/* Разбивает URI на части по разделителю '/' */
vector<string> parseURI(const string& uri) {
    vector<string> parts(0);

    size_t start = 1;
    size_t end = 0;
    while ((end = uri.find('/', start)) != string::npos) {
        string part = uri.substr(start, end - start);
        start += part.length() + 1;
        parts.push_back(part);
    }

    return parts;
}

/* Проверяет можно ли сделать cast строки к uint */
bool containsOnlyDigits(const string& str) {
    bool result = std::all_of(str.begin(), str.end(), [](char c) { return std::isdigit(c); });
    return result;
}

/* Проверяет на совпадение переданной части URI с паттерном.
   Если часть - это queryParam, валидирует и записывает в any.
   Пример queryParam: <id:int>, <name:str> */
tuple<bool, string, any> matchArg(const string& argPattern, const string& arg) {
    bool result = false;

    size_t pos = 0;
    any argValue;
    string argName{};

    if ((pos = argPattern.find(':')) != string::npos) {
        size_t substrLen = argPattern.find('>') - pos - 1;
        string argType = argPattern.substr(pos + 1, substrLen);
        argName = argPattern.substr(argPattern.find('<') + 1, pos - 1);

        /* Валидация аргументов и их запись в any */
        if (argType == "int" && containsOnlyDigits(arg)) {
            argValue = std::stoi(arg);
            result = true;
        } else if (argType == "str") {
            argValue = arg;
            result = true;
        }
    } else {
        result = (argPattern == arg);
    }

    return std::make_tuple(result, argName, argValue);
}
#include <iostream>
/* Проверяет URI на совпадение с переданным паттерном.
   Если в URI были queryParams, то валидирует их и записывает в map. */
tuple<bool, map<string, any>> matchURIAndGetQueryParams(const string& uriPattern, const string& uri) {
    bool result = true;

    vector<string> patternParts = parseURI(uriPattern);
    vector<string> uriParts = parseURI(uri);
    map<string, any> queryParams{};

    if (patternParts.size() == uriParts.size()) {
        for (int i = 0; result && i < patternParts.size(); i++) {
            string argName;
            any argValue;
            std::tie(result, argName, argValue) = matchArg(patternParts[i], uriParts[i]);
            if (result) {
                queryParams[argName] = argValue;
            }
        }
    } else result = false;

    return std::make_tuple(result, queryParams);
}
