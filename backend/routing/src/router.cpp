#include "router.hpp"

#include "goal_controllers.hpp"
#include "parsers.hpp"
#include "index_controller.hpp"
#include "user_controllers.hpp"

Router::Router(const HTTP::Request &request) : request(request), queryParams({}) {
    chooseController(request);
}

void Router::chooseController(const HTTP::Request &request) {
    bool isMatch = false;
    map<string, any> queryParams;
    if (std::get<0>(std::tie(isMatch, queryParams) = matchURIAndGetQueryParams("/register/", request.uri))) {
        this->queryParams = queryParams;
        controller = new RegisterController();

    } else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/login/", request.uri))) {
        this->queryParams = queryParams;
        controller = new LoginController();

    } else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/profile/<user_id:int>/", request.uri))) {
        this->queryParams = queryParams;
        controller = new UserProfileController();

    } else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/profile/", request.uri))) {

        this->queryParams = queryParams;
        controller = new UserProfileController();
    } else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/goal/<goal_id:int>/report/", request.uri))) {
        std::cout << "IN first match: " << std::endl; //DEBUG
        this->queryParams = queryParams;
        controller = new StrikeController();
    } else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/goal/<goal_id:int>/record/", request.uri))) {
        this->queryParams = queryParams;
        controller = new RecordController();
    } else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/goal/<goal_id:int>/leave/", request.uri))) {
        this->queryParams = queryParams;
        controller = new GoalLeaveController();
    } else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/goal/<goal_id:int>/enter/", request.uri))) {
        this->queryParams = queryParams;
        controller = new GoalEnterController();

    } else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/goal/<goal_id:int>/", request.uri))) {
        std::cout << "IN concrete goal: " << std::endl; //DEBUG
        this->queryParams = queryParams;
        controller = new GoalController();
    } 
     else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/goal/", request.uri))) {
        this->queryParams = queryParams;
        controller = new GoalController();
    } 
    else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/index/", request.uri))) {
        this->queryParams = queryParams;
        controller = new IndexController();
    } else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/goal/<goal_id:int>/records/", request.uri))) {
        this->queryParams = queryParams;
        controller = new GoalRecordsController();
    } else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/user/goals/", request.uri))) {
        this->queryParams = queryParams;
        controller = new UserGoalsController();
    } else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/user/records/", request.uri))) {
        this->queryParams = queryParams;
        controller = new UserRecordsController();
    } else if (std::get<0>(std::tie(isMatch, queryParams) =
                               matchURIAndGetQueryParams("/user/reports/", request.uri))) {
        this->queryParams = queryParams;
        controller = new UserReportsController();
    } else {
        controller = nullptr;
    }
}

HTTP::Response Router::Execute() {
    if (!controller) {
        return HTTP::Response(HTTP::NOT_FOUND, {}, string("{}"));
    }
    HTTP::Response response = controller->execute(request, queryParams);
    while (!response.body["redirect"].isNull()) {
        HTTP::Request new_request = request;
        new_request.uri = response.body["redirect"].asString();
        chooseController(new_request);
        response = controller->execute(request, queryParams);
    }

    return response;
}
