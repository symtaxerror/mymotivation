#include <any>
#include <map>
#include <string>
#include <tuple>
#include <vector>
using std::any;
using std::map;
using std::string;
using std::tuple;
using std::vector;

tuple<bool, map<string, any>> matchURIAndGetQueryParams(const string& uriPattern, const string& uri);
