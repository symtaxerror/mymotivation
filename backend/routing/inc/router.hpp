#pragma once

#include "api.hpp"
#include "icontroller.hpp"

class Router {
   public:
    Router(const HTTP::Request& request);
    HTTP::Response Execute();

   private:
    const HTTP::Request& request;
    IController* controller;
    map<string, any> queryParams;
    void chooseController(const HTTP::Request& request);
};
