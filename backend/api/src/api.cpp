#include "api.hpp"

#include <sstream>

HTTP::Request::Request(const string& method, const string& uri, const string& queryString,
                       const map<string, string>& headers, const string& stringBody)
    : method(method), uri(uri), queryString(queryString), headers(headers), isAuthenticated(false), user(-1) {
    std::istringstream sstream(stringBody);
    try {
        sstream >> body;
    } catch (Json::Exception) {
        std::istringstream("{}") >> body;
    }
}

HTTP::Response::Response(ResponseCode statusCode, const map<string, string>& headers,
                         const string& stringBody)
    : statusCode(statusCode), headers(headers) {
    std::istringstream sstream(stringBody);
    sstream >> body;
}
