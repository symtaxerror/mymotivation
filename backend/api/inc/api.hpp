#pragma once

#include <jsoncpp/json/json.h>

#include <map>
#include <string>
using std::map;
using std::string;

namespace HTTP {

struct Request {
    string method;
    string uri;
    string queryString;
    map<string, string> headers;
    Json::Value body;
    bool isAuthenticated;
    int user;
    void set_uri(string new_uri) { uri = new_uri; }
    Request(const string &method, const string &uri, const string &queryString,
            const map<string, string> &headers, const string &stringBody);
    Request(const string &method, const string &uri, const string &queryString,
            const map<string, string> &headers, const Json::Value &body)
        : method(method),
          uri(uri),
          queryString(queryString),
          headers(headers),
          body(body),
          isAuthenticated(false),
          user(-1) {}
};

enum ResponseCode {
    OK = 200,
    UNAUTHORIZED = 401,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    INTERNAL_SERVER_ERROR = 500
};

struct Response {
    ResponseCode statusCode;
    map<string, string> headers;
    Json::Value body;

    Response() {}
    Response(ResponseCode statusCode, const map<string, string> &headers, const string &stringBody);
    Response(ResponseCode statusCode, const map<string, string> &headers, const Json::Value &body)
        : statusCode(statusCode), headers(headers), body(body) {}
};

}  // namespace HTTP
