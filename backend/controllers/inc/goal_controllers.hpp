#pragma once

#include "icontroller.hpp"

class GoalController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request& request, map<string, any> queryParams) override;
};

class GoalEnterController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request& request, map<string, any> queryParams) override;
};

class GoalLeaveController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request& request, map<string, any> queryParams) override;
};

class GoalRecordsController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request& request, map<string, any> queryParams) override;
};

class RecordController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request& request, map<string, any> queryParams) override;
};

class StrikeController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request& request, map<string, any> queryParams) override;
};
