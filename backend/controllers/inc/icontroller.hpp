#pragma once

#include <any>
using std::any;

#include "api.hpp"
#include "interface_db.h"
#include "proxy_db.h"


class IController {
   public:
    IController() { proxy = new proxy_db(); }
    virtual ~IController() {}

   public:
    virtual HTTP::Response execute(const HTTP::Request& request, map<string, any> queryParams) = 0;

   protected:
    interface_db* proxy;
    HTTP::Response un_auth(const HTTP::Request& request) {
        HTTP::Response response(HTTP::ResponseCode::UNAUTHORIZED, { {} }, string("{\"comment\":\"UNAUTHORIZED\"}"));
        return response;
    }
    void redirect(HTTP::Response& response, string redirect) {
        response.body["redirect"] = redirect;
    }
};
