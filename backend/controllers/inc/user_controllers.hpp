#pragma once

#include "icontroller.hpp"

class RegisterController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request &request, map<string, any> queryParams) override;
};

class LoginController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request &request, map<string, any> queryParams) override;
};

class UserProfileController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request &request, map<string, any> queryParams) override;
};

class UserGoalsController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request &request, map<string, any> queryParams) override;
};

class UserRecordsController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request &request, map<string, any> queryParams) override;
};

class UserReportsController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request &request, map<string, any> queryParams) override;
};