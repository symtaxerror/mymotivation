#pragma once

#include "icontroller.hpp"

class IndexController : public IController {
   public:
    HTTP::Response execute(const HTTP::Request &request, map<string, any> queryParams) override;
};
