#include "user_controllers.hpp"

#include <iostream>

#include "user.hpp"

#include "jwt_scheme.hpp"

HTTP::Response RegisterController::execute(const HTTP::Request &request, map<string, any> queryParams)
{
    HTTP::Response response;

    if (request.isAuthenticated)
    {
        response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                  string("{}"));
        redirect(response, "/index/");
        return response;
    }
    if (request.method == "GET")
    {
        response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                  string("{\"render\":[\"<form>Registration</form>\"]}"));
    }
    else if (request.method == "POST")
    {
        User user(request.body);
        Json::Value options;
        options["model"] = USER;
        Json::Value response_json = proxy->Create(&user, options);
        if (response_json["status"].asBool())
        {
            Json::Value pyaload;
            pyaload["id"] = response_json["id"].asInt();
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                      string("{}"));
            response.body["token"] = generateToken(pyaload);
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      response_json["error"].asString()); // ERROR CODE
        }
    }

    return response;
}

HTTP::Response LoginController::execute(const HTTP::Request &request, map<string, any> queryParams)
{
    HTTP::Response response;
    Json::Value response_json;
    User user(request.body);
    Json::Value options;
    options["model"] = USER;
    options["params"] = "LOGIN";

    if (request.isAuthenticated)
    {
        if (request.method == "POST")
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                      string("{\"comment\":\"User was logout\"}"));
        }
    }
    else
    {
        if (request.method == "GET")
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                      string("{\"render\":[\"<form>Login</form>\"]}"));
        }
        else if (request.method == "POST")
        {
            response_json = proxy->SelectOne(&user, options);
            if (response_json["status"].asBool() && !response_json["empty"].asBool())
            {
                std::cout << "Password1:" << response_json["password"].asString() << std::endl; //DEBUG
                std::cout << "Password2:" << user.ToJson()["password"].asString() << std::endl; //DEBUG

                if ((response_json["password"].asString() == user.ToJson()["password"].asString()))
                {
                
                    response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{\"comment\":\"Login succesfull\"}"));
                    response.body["token"] = generateToken(response_json);
                }
                else
                {
                    response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                              string("{\"error\":\"Wrong login or password\"}"));
                }
            }
            else
            {   
                 std::cout << "Password1:" << response_json["login"].asString() << std::endl; //DEBUG
                //пользователя нет
                response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                          string("{\"error\":\"Wrong login or password\"}")); // ERROR CODE
            }
        }
    }
    return response;
}

HTTP::Response UserProfileController::execute(const HTTP::Request &request, map<string, any> queryParams)
{
    /*
    GET / Просмотр своего профиля (только для авторизованных)
    POST / Изменение своего профиля (только для авторизованных)
    DELETE / Удаление своего профиля (только для создавшего)

    GET /<user_id>/ Просмотр чужого профиля (для всех)
    */

    HTTP::Response response;
    Json::Value options;
    Json::Value response_json;

    options["model"] = USER;
    if (request.method == "GET" && request.uri != "/profile/")
    {
<<<<<<< HEAD
=======
        std::cout << "URI: " << request.uri << std::endl; //DEBUG
        for (auto v : queryParams)
        {
            std::cout << v.first << std::endl;
        }
>>>>>>> 4919d7169fdc6a5e802efcdad4e9c2bb6087c2ef
        int user_id = std::any_cast<int>(queryParams["user_id"]);
        User user(user_id);
        response_json = proxy->SelectOne(&user, options);
        if (response_json["status"].asBool() && !response_json["empty"].asBool())
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                      string("{\"render\":[\"<h1>" + response_json["email"].asString() +
                                             "</h1><p> Last name: " + response_json["lastName"].asString() +
                                             "\nFirts name: " + response_json["firstName"].asString() + "</p> \"]}")); // + other information about user
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      string("{\"comment\":\"Profile not found\"}"));
        }
        return response;
    }

    if (!request.isAuthenticated)
    {
        return un_auth(request);
    }

    User user(request.user);
    if (request.method == "GET")
    {
        response_json = proxy->SelectOne(&user, options);
        std::cout << "\nTYT\n" << std::endl;
        if (response_json["status"].asBool() && !response_json["empty"].asBool())
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                      string("{\"render\":[\"<h1>" + response_json["email"].asString() +
                                             "</h1><p> Last name: " + response_json["lastName"].asString() +
                                             "\nFirts name: " + response_json["firstName"].asString() + "</p> \"]}")); // + other information about user
        }
    }
    else if (request.method == "POST")
    {
        response_json = proxy->SelectOne(&user, options);
        if (response_json["status"].asBool() && !response_json["empty"].asBool())
        {
            Json::Value fields_change("{}");
            for (string field : user.fields)
            {
                if (user.ToJson()[field].asString() != response_json[field].asString())
                {
                    fields_change[field] = response_json[field];
                }
                else
                {
                    fields_change[field] = user.ToJson()[field];
                }
            }
            user.FromJson(fields_change);
            response_json = proxy->Update(&user, options);
            if (response_json["status"].asBool())
            {
                response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{\"comment\":\"Changed fields\"}"));
            }
            else
            {
                response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                          response_json["error"].asString());
            }
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      response_json["error"].asString());
        }
    }

    else if (request.method == "DELETE")
    {
        response_json = proxy->Delete(&user, options);
        if (response_json["status"].asBool())
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{\"comment\":\"User deleted\"}"));
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      response_json["error"].asString());
        }
    }

    return response;
}

HTTP::Response UserGoalsController::execute(const HTTP::Request &request, map<string, any> queryParams)
{
    if (!request.isAuthenticated)
    {
        return un_auth(request);
    }

    Json::Value options;
    options["model"] = USER;
    options["params"] = "GOALS";
    User user(request.user);
    HTTP::Response response;
    Json::Value response_json;

    if (request.method == "GET")
    {
        response_json = proxy->SelectMany(&user, options);
        if (response_json["status"].asBool() && !response_json["empty"].asBool())
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{}"));
            for (auto const &key : response_json.getMemberNames())
            {
                if (key != "status" && key != "empty")
                {
                    response.body[key] = response_json[key];
                }
            }
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      string("{\"comment\":\"You dont enter at any goal\"}"));
        }
    }
    return response;
}

HTTP::Response UserRecordsController::execute(const HTTP::Request &request, map<string, any> queryParams)
{
    if (!request.isAuthenticated)
    {
        return un_auth(request);
    }

    Json::Value options;
    options["model"] = USER;
    options["params"] = "RECORDS";
    User user(request.user);
    HTTP::Response response;
    Json::Value response_json;

    if (request.method == "GET")
    {
        response_json = proxy->SelectMany(&user, options);
        if (response_json["status"].asBool() && !response_json["empty"].asBool())
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{}"));
            for (auto const &key : response_json.getMemberNames())
            {
                if (key != "status" && key != "empty")
                {
                    response.body[key] = response_json[key];
                }
            }
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      string("{\"comment\":\"You dont have records\"}"));
        }
    }
    return response;    
}

HTTP::Response UserReportsController::execute(const HTTP::Request &request, map<string, any> queryParams)
{
    if (!request.isAuthenticated)
    {
        return un_auth(request);
    }

    Json::Value options;
    options["model"] = USER;
    options["params"] = "REPORTS";
    User user(request.user);
    HTTP::Response response;
    Json::Value response_json;

    if (request.method == "GET")
    {
        response_json = proxy->SelectMany(&user, options);
        if (response_json["status"].asBool() && !response_json["empty"].asBool())
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{}"));
            for (auto const &key : response_json.getMemberNames())
            {
                if (key != "status" && key != "empty")
                {
                    response.body[key] = response_json[key];
                }
            }
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      string("{\"comment\":\"You dont send any reports\"}"));
        }
    }
    return response;    
}