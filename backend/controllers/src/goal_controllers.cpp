#include "goal_controllers.hpp"

#include "goal.hpp"
#include "record.hpp"
#include "strike.hpp"

HTTP::Response GoalController::execute(const HTTP::Request &request, map<string, any> queryParams)
{
    /*
        GET / Форма создания новой цели (только для авторизованных)
        POST / Создание новой цели (только для авторизованных)

        GET /<goal_id>/ Просмотр цели
        POST /<goal_id>/ Изменение цели (только для создавшего)
        DELETE /<goal_id>/ Удаление цели (только для создавшего)
     */

    HTTP::Response response;
    Json::Value response_json;
    Json::Value options;
    options["model"] = GOAL;

    if (request.uri == "/goal/")
    {
        if (!request.isAuthenticated)
        {
            return un_auth(request);
        }
        Goal goal(request.body);
        goal.SetUserCreatorId(request.user);
        if (request.method == "POST")
        {
            response_json = proxy->Create(&goal, options);
            options["model"] = GOAL;
            options["params"] = "USER";
            goal.FromJson(response_json);
            std::cout << response_json["id"].asInt() << std::endl;
            goal.SetUserId(request.user);
            std::cout << request.user<< std::endl;
            response_json = proxy->Update(&goal, options); //herego
            if (response_json["status"].asBool() && !response_json["empty"].asBool())
            {
                response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{\"comment\":\"Goal created and enter\"}"));
            }
            else
            {
                response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                          response_json["error"].asString());
            }
        }
        else if (request.method == "GET")
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{\"render\":[\"<form>Goal</form>\"]}"));
        }
    }
    else
    {
        int goal_id = std::any_cast<int>(queryParams["goal_id"]);
        Goal goal(goal_id);
        if (request.method == "GET")
        {
            response_json = proxy->SelectOne(&goal, options);
            if (response_json["status"].asBool() && !response_json["empty"].asBool())
            {
                response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{\"render\":[\"<h1>" + response_json["title"].asString() + "</h1>\"]}")); //+other goal inf
                response.body["Goal: "] = response_json;
            }
            else
            {
                response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                          response_json["error"].asString());
            }
        }

        if (!request.isAuthenticated)
        {
            return un_auth(request);
        }

        if (request.method == "POST")
        {

            Json::Value fields_change("{}");
            if (goal.ToJson()["creatorUserId"].asInt() == request.user)
            {
                response_json = proxy->SelectOne(&goal, options);
                if (response_json["status"].asBool() && !response_json["empty"].asBool())
                {
                    for (string field : goal.fields)
                    {
                        if (goal.ToJson()[field].asString() != response_json[field].asString())
                        {
                            fields_change[field] = response_json[field];
                        }
                        else
                        {
                            fields_change[field] = goal.ToJson()[field];
                        }
                    }
                    goal.FromJson(fields_change);
                    proxy->Update(&goal, options);
                    response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{\"comment\":\"Goal updated\"}"));
                }
                else
                {
                    response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                              response_json["error"].asString());
                }
            }
            else
            {
                response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}}, string("{\"comment\":\"User not a creator\"}"));
            }
        }
        else if (request.method == "DELETE")
        {
            options["params"] = "DEFAULT";
            response_json = proxy->Delete(&goal, options);
            if (response_json["status"].asBool())
            {
                response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{\"comment\":\"Deleted succesfull\"}")); //+other goal inf
                redirect(response, "/index/");
            }
            else
            {
                response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                          response_json["error"].asString());
            }
        }
    }
    return response;
}

HTTP::Response StrikeController::execute(const HTTP::Request &request, map<string, any> queryParams)
{

    if (!request.isAuthenticated)
    {
        return un_auth(request);
    }

    Json::Value response_json;
    HTTP::Response response;
    Json::Value options;
    options["model"] = STRIKE;
    int goal_id = std::any_cast<int>(queryParams["goal_id"]);
    Strike strike(request.body);
    strike.SetFromId(request.user);

    if (request.method == "POST")
    {
        response_json = proxy->Create(&strike, options);
        if (response_json["status"].asBool())
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                      string("{\"comment\":\"Your strike was sended\"}"));
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      response_json["error"].asString());
        }
    }
    else if (request.method == "GET")
    {
        response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                  string("{\"render\":\"<form>Report form</form>\"}"));
    }

    return response;
}

HTTP::Response GoalEnterController::execute(const HTTP::Request &request, map<string, any> queryParams)
{
    if (!request.isAuthenticated)
    {
        return un_auth(request);
    }
    Json::Value response_json;
    HTTP::Response response;
    Json::Value options;
    options["model"] = GOAL;
    options["params"] = "USER";
    int goal_id = std::any_cast<int>(queryParams["goal_id"]);
    Goal goal(goal_id);

    if (request.method == "POST")
    {
        response_json = proxy->SelectOne(&goal, options);
        std::string title = response_json["title"].asString();
        if (response_json["status"].asBool() && !response_json["empty"].asBool())
        {
            goal.FromJson(response_json);
            goal.SetUserId(request.user);
            response_json = proxy->Update(&goal, options); //herego2
            if (response_json["status"].asBool())
            {
                response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                          string("{\"comment\":\"You enter to goal " + title + "\"}"));
            }
            else
            {
                response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                          response_json["error"].asString());
            }
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      response_json["error"].asString());
        }
    }
    return response;
}

HTTP::Response GoalLeaveController::execute(const HTTP::Request &request, map<string, any> queryParams)
{
    if (!request.isAuthenticated)
    {
        return un_auth(request);
    }

    Json::Value response_json;
    HTTP::Response response;
    Json::Value options;
    options["model"] = GOAL;
    options["params"] = "USER";
    int goal_id = std::any_cast<int>(queryParams["goal_id"]);
    Goal goal(goal_id);

    if (request.method == "POST")
    {
        response_json = proxy->SelectOne(&goal, options);
        std::string title = response_json["title"].asString();
        if (response_json["status"].asBool() && !response_json["empty"].asBool())
        {
            goal.FromJson(response_json);
            goal.SetUserId(request.user);
            response_json = proxy->Delete(&goal, options);
            if (response_json["status"].asBool() && !response_json["empty"].asBool())
            {
                response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                          string("{\"comment\":\"Leave from goal" + title + "\"}"));
            }
            else
            {
                response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                          response_json["error"].asString()); // ERROR CODE
            }
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      response_json["error"].asString()); // ERROR CODE
        }
    }
    return response;
}

HTTP::Response RecordController::execute(const HTTP::Request &request, map<string, any> queryParams)
{
    if (!request.isAuthenticated)
    {
        return un_auth(request);
    }
    Json::Value response_json;
    HTTP::Response response;
    Json::Value options;
    options["model"] = RECORD;
    int goal_id = std::any_cast<int>(queryParams["goal_id"]);
    Record record(request.body);
    record.setGoalId(goal_id);
    std::cout << request.user << std::endl;
    record.setUserId(request.user);

    if (request.method == "POST")
    {
        //TODO: Проверить, что состаю в цели
        response_json = proxy->Create(&record, options);
        if (response_json["status"].asBool())
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                      string("{\"comment\":\"Your record was sented\"}"));
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      response_json["error"].asString());
        }
    }
    else if (request.method == "GET")
    {
        response = HTTP::Response(HTTP::ResponseCode::OK, {{}},
                                  string("{\"render\":\"<form>Record form</form>\"}"));
    }
    return response;
}

HTTP::Response GoalRecordsController::execute(const HTTP::Request &request, map<string, any> queryParams)
{
    Json::Value options;
    options["model"] = GOAL;
    options["params"] = "RECORDS"; // USERS, RECORDS, DEFAULT
    int goal_id = std::any_cast<int>(queryParams["goal_id"]);
    Goal goal(goal_id);
    HTTP::Response response;
    Json::Value response_json;

    if (request.method == "GET")
    {
        response_json = proxy->SelectMany(&goal, options);
        if (response_json["status"].asBool() && !response_json["empty"].asBool())
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{}"));
            options["model"] = RECORD;
            options["params"] = "DEFAULT";
            for (auto const &key : response_json.getMemberNames())
            {
                if (key != "status" && key != "empty")
                {
                    Record tmp(stoi(key));
                    response.body[key] = Record(proxy->SelectOne(&tmp, options)).ToJson();
                }
            }
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      response_json["error"].asString());
        }
    }
    return response;
}