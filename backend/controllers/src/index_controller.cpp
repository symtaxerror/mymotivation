#include "index_controller.hpp"
#include "goal.hpp"
#include "api.hpp"

HTTP::Response IndexController::execute(const HTTP::Request &request, map<string, any> queryParams)
{
    Json::Value options;
    options["model"] = GOAL;
    options["params"] = "DEFAULT"; // USERS, RECORD, DEFAULT
    Goal goal(0);
    HTTP::Response response;
    Json::Value response_json;

    if (request.method == "GET")
    {
        response_json = proxy->SelectMany(&goal, options);
        if (response_json["status"].asBool() && !response_json["empty"].asBool())
        {
            response = HTTP::Response(HTTP::ResponseCode::OK, {{}}, string("{}"));
            for (auto const &key : response_json.getMemberNames())
            {
                if (key != "status" && key != "empty")
                {
                    Goal goal(stoi(key));
                    Json::Value clearGoal;
                    Json::Value tmp = Goal(proxy->SelectOne(&goal, options)).ToJson();
                    clearGoal["id"] = tmp["id"].asInt();
                    clearGoal["title"] = tmp["title"].asString();
                    clearGoal["description"] = tmp["description"].asString();
                    response.body[key] = clearGoal;
                }
            }
        }
        else
        {
            response = HTTP::Response(HTTP::ResponseCode::NOT_FOUND, {{}},
                                      string("{\"comment\":\"No goals\"}"));
        }
    }
    return response;
}