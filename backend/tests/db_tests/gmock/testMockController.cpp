#include "gmock/gmock.h"

#include "DataBaseControler.h"
#include "BaseDB.h"
#include "wrapperDb.h"

using std::string;
using std::map;

class MockDB : public BaseDB
{
public:
    MOCK_METHOD0(Create, int());
};

using ::testing::AtLeast;
TEST(PainterTest, CanDrawSomething) {
    MockDB testDB;
    EXPECT_CALL(testDB, Create())
            .Times(AtLeast(1));

    DataBaseController controller(&testDB);

    map<string, string> userData = {{"id", "1"},
                                    {"first", "Ivan"},
                                    {"lastName", "Ivanov"},
                                    {"mail", "@mail.ru"},
                                    {"password", " asdasa"},
                                    {"balance", "100"}};

    WrapperDb wrap;
    wrap.setWrap(USER, userData);
    controller.Create(wrap);
}

