#include "gtest/gtest.h"

extern "C" {
#include "DBReport.h"
#include <cstdlib>
}
using std::string;
using std::map;

TEST (DBReport, createAndSelectOne)
{
    int id = 1;
    string status;
    string description = "some text";

    DBReport * report = new DBReport();
    report->setParameters(id, status, description);
    report->Create();
    delete report;

    DBReport * savedReport = new DBReport();
    savedReport->setId(id);
    map<string, string> dataFromDb = savedReport->SelectOne();
    delete savedReport;

    ASSERT_NE (dataFromDb.end(),dataFromDb.find("id"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("status"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("description"));

    EXPECT_EQ (id, atoi(dataFromDb.find("id")->second.c_str()));
    EXPECT_EQ (status, dataFromDb.find("status")->second);
    EXPECT_EQ (description, dataFromDb.find("description")->second);
}

TEST (DBReport, del)
{
    int id = 1;
    string status;
    string description = "some text";

    DBReport * report = new DBReport();
    report->setParameters(id, status, description);
    report->Create();
    delete report;

    DBReport * reportDel = new DBReport();
    reportDel->setId(id);
    reportDel->Delete();
    delete reportDel;

    DBReport * savedReport = new DBReport();
    savedReport->setId(id);
    map<string, string> dataFromDb = savedReport->SelectOne();
    delete savedReport;

    ASSERT_NE (dataFromDb.end(),dataFromDb.find("status"));
    EXPECT_EQ ("err", dataFromDb.find("status")->second);
}

TEST (DBReport, update)
{
    int id = 1;
    string status;
    string description = "some text";

    DBReport * report = new DBReport();
    report->setParameters(id, status, description);
    report->Create();
    delete report;

    DBReport * savedReport = new DBReport();
    description = "new text";
    savedReport->setParameters(id, status, description);
    savedReport->Update();
    delete savedReport;

    DBReport * updatedReport = new DBReport();
    updatedReport->setId(id);
    map<string, string> dataFromDb = updatedReport->SelectOne();
    delete updatedReport;

    ASSERT_NE (dataFromDb.end(),dataFromDb.find("id"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("status"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("description"));

    EXPECT_EQ (id, atoi(dataFromDb.find("id")->second.c_str()));
    EXPECT_EQ (status, dataFromDb.find("status")->second);
    EXPECT_EQ (description, dataFromDb.find("description")->second);
}

