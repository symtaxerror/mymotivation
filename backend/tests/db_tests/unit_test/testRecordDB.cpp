#include "gtest/gtest.h"

extern "C" {
#include "DBRecord.h"
#include <cstdlib>
}
using std::string;
using std::map;


TEST (DBRecord, createAndSelectOne)
{
    int id = 1;
    string description = "someText";
    int date = 123123;
    int reportCounter = 132;
    string reportStatus = "someStatus";

    DBRecord * user = new DBRecord();
    user->setParameters(id, description, date, reportCounter, reportStatus);
    user->Create();
    delete user;

    DBRecord * savedUser = new DBRecord();
    user->setId(id);
    map<string, string> dataFromDb = savedUser->SelectOne();
    delete savedUser;

    ASSERT_NE (dataFromDb.end(),dataFromDb.find("id"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("description"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("date"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("reportCounter"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("reportStatus"));


    EXPECT_EQ (id, atoi(dataFromDb.find("id")->second.c_str()));
    EXPECT_EQ (description, dataFromDb.find("description")->second);
    EXPECT_EQ (date, atoi(dataFromDb.find("date")->second.c_str()));
    EXPECT_EQ (reportCounter, atoi(dataFromDb.find("reportCounter")->second.c_str()));
    EXPECT_EQ (reportStatus, dataFromDb.find("reportStatus")->second);
}

TEST (DBRecord, del)
{
    int id = 1;
    string description = "someText";
    int date = 123123;
    int reportCounter = 132;
    string reportStatus = "someStatus";

    map <string, string> :: iterator it, it_2;
    DBRecord * record = new DBRecord();
    record->setParameters(id, description, date, reportCounter, reportStatus);
    record->Create();
    delete record;

    DBRecord * recordDel = new DBRecord();
    recordDel->setId(id);
    recordDel->Delete();
    delete recordDel;

    DBRecord * deletedRecord = new DBRecord();
    deletedRecord->setId(id);
    map<string, string> dataFromDb = deletedRecord->SelectOne();
    delete deletedRecord;

    ASSERT_NE (dataFromDb.end(),dataFromDb.find("status"));
    EXPECT_EQ ("err", dataFromDb.find("status")->second);
}

TEST (DBRecord, update)
{
    int id = 1;
    string description = "someText";
    int date = 123123;
    int reportCounter = 132;
    string reportStatus = "someStatus";

    DBRecord * record = new DBRecord();
    record->setParameters(id, description, date, reportCounter, reportStatus);
    record->Create();
    delete record;

    DBRecord * savedRecord = new DBRecord();
    reportCounter = 200;
    savedRecord->setParameters(id, description, date, reportCounter, reportStatus);
    savedRecord->Update();
    delete savedRecord;

    DBRecord * updatedRecord = new DBRecord();
    updatedRecord->setId(id);
    map<string, string> dataFromDb = updatedRecord->SelectOne();
    delete updatedRecord;

    ASSERT_NE (dataFromDb.end(),dataFromDb.find("id"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("description"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("date"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("reportCounter"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("reportStatus"));

    EXPECT_EQ (id, atoi(dataFromDb.find("id")->second.c_str()));
    EXPECT_EQ (description, dataFromDb.find("description")->second);
    EXPECT_EQ (date, atoi(dataFromDb.find("date")->second.c_str()));
    EXPECT_EQ (reportCounter, atoi(dataFromDb.find("reportCounter")->second.c_str()));
    EXPECT_EQ (reportStatus, dataFromDb.find("reportStatus")->second);
}
