#include <pqxx/pqxx>
#include "iostream"
using std::string;
int initDB()
{
    pqxx::connection *connection = new pqxx::connection("postgresql://dev:dev@localhost/mydb");
    pqxx::work *work = new pqxx::work(*connection);
    string creteUser = "CREATE TABLE USERS("
                       "id SERIAL PRIMARY KEY NOT NULL,"
                       "firstName CHAR(25) NOT NULL,"
                       "email CHAR(25) NOT NULL,"
                       "lastName CHAR(25),"
                       "login CHAR(50),"
                       "password CHAR(25),"
                       "date DATE DEFAULT CURRENT_DATE );";

    string creteGoal = "CREATE TABLE GOALS("
                       "id SERIAL PRIMARY KEY NOT NULL,"
                       "title CHAR(25) NOT NULL,"
                       "description TEXT NOT NULL);";

    string createUserToGoal = "CREATE TABLE USERTOGOAL("
                              "idUser INT ,"
                              "idGoal INT ,"
                              "isCreator BOOLEAN DEFAULT FALSE ,"
                              "FOREIGN KEY (idUser) REFERENCES USERS(id) ON DELETE CASCADE,"
                              "FOREIGN KEY (idGoal) REFERENCES GOALS(id) ON DELETE CASCADE,"
                              "CONSTRAINT pk PRIMARY KEY (idUser, idGoal) );";


    string createUserToReport = "CREATE TABLE USERTOREPORT("
                                "idUser INT ,"
                                "idReport INT ,"
                                "FOREIGN KEY (idUser) REFERENCES USERS(id) ON DELETE CASCADE,"
                                "FOREIGN KEY (idReport) REFERENCES REPORTS(id) ON DELETE CASCADE,"
                                "CONSTRAINT utr PRIMARY KEY (idUser, idReport) );";

    string createRecord = "CREATE TABLE RECORDS("
                          "id SERIAL PRIMARY KEY NOT NULL,"
                          "description TEXT NOT NULL,"
                          "reportStatus BOOLEAN DEFAULT false,"
                          "idUser INT NOT NULL  REFERENCES USERS(id) ON DELETE CASCADE,"
                          "idGoal INT NOT NULL  REFERENCES GOALS(id) ON DELETE CASCADE);";

    string createReport = "CREATE TABLE REPORTS("
                          "id SERIAL PRIMARY KEY NOT NULL,"
                          "fromId INT NOT NULL REFERENCES USERS(id) ON DELETE CASCADE,"
                          "description TEXT NOT NULL,"
                          "status CHAR(25) DEFAULT 'wait' ,"
                          "recordId INT NOT NULL  REFERENCES RECORDS(id) ON DELETE CASCADE);";

    try
    {
        work->exec(creteUser.c_str());
        work->exec(creteGoal.c_str());
        work->exec(createRecord.c_str());
        work->exec(createReport.c_str());
        work->commit();
        work = new pqxx::work(*connection);
        work->exec(createUserToGoal.c_str());
        work->exec(createUserToReport.c_str());
        work->commit();
        return 0;
    }
    catch (std::exception)
    {
        return 1;
    }
}
