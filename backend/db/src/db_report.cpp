#include "db_report.h"
db_report::db_report( pqxx::connection * connection ):base_db( connection )
{
    connection->prepare("insert_to_report", "insert into REPORTS "
                                            "(fromId, description, status, recordId)"
                                            " values ($1, $2, $3, $4);");

    connection->prepare("select_last_report", "SELECT * FROM REPORTS ORDER BY id desc LIMIT 1;");
    connection->prepare("delete_report", "DELETE FROM REPORTS WHERE id=$1;");

    work = new pqxx::work(*connection);
}
Json::Value db_report::Create(BaseModel * bs)
{
    Json::Value resultJson;
    pqxx::result res;
    Json::Value data = bs->ToJson();
    try
    {
        work->prepared("insert_to_report")(data["fromId"].asString())
                (data["description"].asString())(data["status"].asString())
                (data["recordId"].asInt()).exec();
        work->commit();
        work = new pqxx::work(*connection);
        res = work->prepared("select_last_report").exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        resultJson["status"] = false;
        return resultJson;
    }
    resultJson["status"] = true;
    resultJson["id"] = atoi( res[0][0].c_str() );

    return resultJson;
}

Json::Value db_report::Delete(BaseModel * bs )
{
    Json::Value data = bs->ToJson();
    Json::Value res;
    try
    {
        work->prepared("delete_report")(data["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        res["status"] = false;
        return res;
    }
    res["id"] = data["id"].asInt();
    res["status"] = true;
    return res;
}

Json::Value db_report::Update( BaseModel * bs )
{

}

Json::Value db_report::SelectOne(BaseModel * bs)
{
    Json::Value sourceJson = bs->ToJson();
    Json::Value resultJson;
    pqxx::result res;
    try
    {
        res = work->prepared("select_one")(sourceJson["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        resultJson["status"] = false;
        return resultJson;
    }
    if( res.empty() )
    {
        resultJson["empty"] = true;
        resultJson["status"] = true;
        return resultJson;
    }

    int i = 0;
    for( auto line : res )
    {
        for( auto key : line )
        {
            resultJson[std::to_string(i++)] = key.c_str();
        }
    }
    resultJson["status"] = true;
    resultJson["empty"] = false;

    return resultJson;
}

Json::Value db_report::SelectMany(BaseModel * bs)
{

}

