#include "proxy_db.h"

proxy_db::proxy_db(): service{new DataBaseController}
{

}

Json::Value proxy_db::Create(BaseModel * bs, const Json::Value & options )
{
    return service->Create(bs, options);
}

Json::Value proxy_db::Delete(BaseModel * bs, const Json::Value & options )
{
    return service->Delete(bs, options);
}

Json::Value proxy_db::SelectOne(BaseModel * bs, const Json::Value & options )
{
    return service->SelectOne(bs, options);
}

Json::Value proxy_db::SelectMany(BaseModel * bs, const Json::Value & options )
{
    return service->SelectMany(bs, options);
}

Json::Value proxy_db::Update(BaseModel * bs, const Json::Value & options )
{
    return service->Update(bs, options);
}

proxy_db::~proxy_db()
{
    delete service;
}