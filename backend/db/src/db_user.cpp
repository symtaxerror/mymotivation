#include "db_user.h"
#include <iostream>
db_user::db_user( pqxx::connection * connection ) : base_db( connection )
{
    connection->prepare("insert_to_user", "insert into USERS "
                                          "(firstName, lastName, email, login, password )"
                                          " values ($1, $2, $3, $4, $5);");
    connection->prepare("delete_user", "DELETE FROM USERS WHERE id=$1;");

    connection->prepare("select_user", "SELECT * FROM USERS WHERE id=$1;");
    connection->prepare("select_user_by_login", "SELECT * FROM USERS WHERE login=$1;");

    connection->prepare("select_last_user", "SELECT * FROM USERS ORDER BY id desc limit 1");
    connection->prepare("select_all_users", "SELECT * FROM USERS ;");
    connection->prepare("update_user", "UPDATE USERS SET "
                                       "firstName =$1, lastName=$2, email=$3, login=$4, password=$5 "
                                       " WHERE id=$6;");

    connection->prepare("select_users_goal", "SELECT g.id, title FROM USERTOGOAL ug "
                                              "JOIN GOALS g on g.id = ug.idGoal "
                                              "JOIN USERS u on u.id = ug.idUser "
                                              "WHERE u.id = $1 ORDER BY g.title;");

    connection->prepare("select_users_report", "SELECT r.id, description FROM USERTOREPORT ur "
                                             "JOIN REPORTS r on r.id = ur.idReport "
                                             "JOIN USERS u on u.id = ur.idReport "
                                             "WHERE u.id = $1 ORDER BY r.description;");
    work = new pqxx::work(*connection);
}

Json::Value db_user::Create(BaseModel * bs)
{
    Json::Value resultJson;
    pqxx::result res;
    Json::Value data = bs->ToJson();
    try
    {
        work->prepared("insert_to_user")
            (data["firstName"].asString())(data["lastName"].asString())
            (data["email"].asString())(data["login"].asString())
            (data["password"].asString()).exec();
        work->commit();
        work = new pqxx::work(*connection);
        res = work->prepared("select_last_user").exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        resultJson["status"] = false;
        return resultJson;
    }
    resultJson["status"] = true;
    resultJson["id"] = atoi(res[0][0].c_str());
    return resultJson;
}

Json::Value db_user::Delete(BaseModel * bs )
{
    Json::Value data = bs->ToJson();
    Json::Value res;
    try
    {
        work->prepared("delete_user")(data["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        res["status"] = false;
        return res;
    }
    res["id"] = data["id"].asInt();
    res["status"] = true;
    return res;
}

Json::Value db_user::Update( BaseModel * bs )
{
    Json::Value res;
    Json::Value data = bs->ToJson();
    try
    {
        work->prepared("update_user")(data["firstName"].asString())(data["lastName"].asString())
                (data["email"].asString())(data["login"].asString())
                (data["password"].asString())(data["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        res["status"] = true;
        return res;
    }
    res["id"] = data["id"].asString();
    res["status"] = true;
    return res;
}

Json::Value db_user::SelectOne(BaseModel * bs)
{
    Json::Value sourceJson = bs->ToJson();
    Json::Value resultJson;
    pqxx::result res;
    try
    {
        if( sourceJson["id"] == -1 )
            res = work->prepared("select_user_by_login")(sourceJson["login"].asString()).exec();
        else
            res = work->prepared("select_user")(sourceJson["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        resultJson["status"] = false;
        return resultJson;
    }
    if( res.empty() )
    {
        resultJson["empty"] = true;
        resultJson["status"] = true;
        return resultJson;
    }
    resultJson["status"] = true;
    resultJson["empty"] = false;

    resultJson["id"] = atoi(res[0][0].c_str());
    resultJson["firstName"] = res[0][1].c_str();
    resultJson["email"] = res[0][2].c_str();
    resultJson["lastName"] = res[0][3].c_str();
    resultJson["login"] = res[0][4].c_str();
    resultJson["password"] = res[0][5].c_str();
    return resultJson;
}

Json::Value db_user::SelectMany(BaseModel * bs)
{
    Json::Value sourceJson = bs->ToJson();
    Json::Value resultJson;
    pqxx::result res;
    try
    {
        res = work->prepared("select_many")(sourceJson["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        resultJson["status"] = false;
        return resultJson;
    }
    if( res.empty() )
    {
        resultJson["empty"] = true;
        resultJson["status"] = true;
        return resultJson;
    }

    resultJson["status"] = true;
    resultJson["empty"] = false;
    for( auto line : res )
    {
        resultJson[line[0].c_str()]["first"] = line[0].c_str();
        resultJson[line[0].c_str()]["second"] = line[1].c_str();
    }

    return resultJson;
}

