#include "interface_db.h"


interface_db::interface_db()
{

}

Json::Value interface_db::Create(BaseModel * bs, const Json::Value & options )
{

}

Json::Value interface_db::SelectOne(BaseModel * bs, const Json::Value & options )
{

}

Json::Value interface_db::SelectMany(BaseModel * bs, const Json::Value & options )
{

}

Json::Value interface_db::Update(BaseModel * bs, const Json::Value & options )
{

}

Json::Value interface_db::Delete(BaseModel * bs, const Json::Value & options )
{

}

interface_db::~interface_db()
{

}