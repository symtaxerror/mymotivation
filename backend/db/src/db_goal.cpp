#include "db_goal.h"


db_goal::db_goal( pqxx::connection * connection ) : base_db( connection )
{
    connection->prepare("insert_goal", "insert into GOALS "
                                          "(title, description)"
                                          " values ($1, $2);");
    connection->prepare("delete_goal", "DELETE FROM GOALS WHERE id=$1;");
    connection->prepare("delete_goals_user", "DELETE FROM USERTOGOAL where idUser=$1 AND idGoal=$2");
    connection->prepare("select_goal", "SELECT * FROM GOALS WHERE id=$1;");
    connection->prepare("select_all_goals", "SELECT * FROM GOALS;");
    connection->prepare("select_last_goal", "SELECT * FROM GOALS ORDER BY id desc limit 1;");


    connection->prepare("select_goals_user", "SELECT u.id, u.firstName "
                                             "FROM USERTOGOAL ug "
                                             "JOIN USERS u  on u.id = ug.idUser "
                                             "JOIN GOALS g on g.id = ug.idGoal "
                                             "WHERE g.id = $1 ORDER BY u.firstName;");

    connection->prepare("update_goal", "UPDATE GOALS SET title =$1,"
                                       " description=$2 WHERE id=$3;");
    connection->prepare("update_goals_user", "insert into USERTOGOAL (idUser, idGoal) "
                                             "values ($1, $2);");

    connection->prepare("select_goals_record", "SELECT r.id, r.description FROM records r "
                                               "JOIN GOALS g on g.id = r.idGoal "
                                               "WHERE g.id = $1;");

    work = new pqxx::work(*connection);
}

Json::Value db_goal::Create(BaseModel * bs)
{
    Json::Value resultJson;
    pqxx::result res;
    Json::Value data = bs->ToJson();
    try
    {
        work->prepared("insert_goal")
                (data["title"].asString())(data["description"].asString()).exec();
        work->commit();
        work = new pqxx::work(*connection);
        res = work->prepared("select_last_goal").exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        resultJson["status"] = false;
        return resultJson;
    }
    resultJson["id"] = atoi(res[0][0].c_str() );
    resultJson["status"] = true;
    return resultJson;
}

Json::Value db_goal::Delete(BaseModel * bs )
{
    Json::Value data = bs->ToJson();
    Json::Value res;
    try
    {
        if( data["userId"].asInt() == -1 )
            work->prepared("delete_goal")(data["id"].asInt()).exec();
        else
            work->prepared("delete_goals_user")(data["userId"].asInt())(data["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        res["status"] = false;
        return res;
    }
    res["id"] = data["id"].asInt();
    res["status"] = true;
    return res;}

Json::Value db_goal::Update( BaseModel * bs )
{
    Json::Value res;
    Json::Value data = bs->ToJson();
    try
    {
        if( data["userId"].asInt() == -1 )
            work->prepared("update_goal")(data["title"].asString())(data["description"].asString())
                (data["id"].asString()).exec();
        else
            work->prepared("update_goals_user")(data["userId"].asInt())(data["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        res["status"] = false;
        return res;
    }
    res["status"] = true;
    res["id"] = data["id"].asString();
    return res;
}

Json::Value db_goal::SelectOne(BaseModel * bs)
{
    Json::Value sourceJson = bs->ToJson();
    Json::Value resultJson;
    pqxx::result res;
    try
    {
        res = work->prepared("select_goal")(sourceJson["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        resultJson["status"] = false;
        return resultJson;
    }
    if( res.empty() )
    {
        resultJson["empty"] = true;
        return resultJson;
    }
    resultJson["status"] = true;
    resultJson["empty"] = false;
    resultJson["id"] = atoi(res[0][0].c_str());
    resultJson["title"] = res[0][1].c_str();
    resultJson["description"] = res[0][2].c_str();
    return resultJson;
}

Json::Value db_goal::SelectMany(BaseModel * bs)
{
    Json::Value sourceJson = bs->ToJson();
    Json::Value resultJson;
    pqxx::result res;
    try
    {
        res = work->prepared("select_many")(sourceJson["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        resultJson["status"] = false;
        return resultJson;
    }
    if( res.empty() )
    {
        resultJson["status"] = true;
        resultJson["empty"] = true;
        return resultJson;
    }

    for( auto line : res )
    {
        resultJson[line[0].c_str()]["first"] = line[0].c_str();
        resultJson[line[0].c_str()]["second"] = line[1].c_str();
    }
    resultJson["status"] = true;
    resultJson["empty"] = false;

    return resultJson;
}

