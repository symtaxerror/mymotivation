#include "data_base_controler.h"

DataBaseController::DataBaseController(): obj(nullptr)
{
    connection = new pqxx::connection("postgresql://dev:dev@localhost/mydb");
}

DataBaseController::DataBaseController(base_db * obj)
{
    this->obj = obj;
}

Json::Value DataBaseController::Create(BaseModel * bs, const Json::Value & options )
{
    switch ( options["model"].asInt() ) {
        case USER:
        {
            obj = new db_user( connection );
            return obj->Create(bs);
        }
        case GOAL:
        {
            obj = new db_goal( connection );
            return obj->Create(bs);
        }
        case RECORD:
        {
            obj = new db_record( connection );
            return obj->Create(bs);
        }
        case STRIKE:
        {
            obj = new db_report( connection );
            return obj->Create(bs);

        }
        case MODERATOR:
        {
            obj = new db_moderator();

        }
        case SESSION:
        {
            obj = new db_session();
        }
        default:
        {
            Json::Value result;
            result["status"] = "err";
            return  result;
        }
    }
}

Json::Value DataBaseController::Delete(BaseModel * bs, const Json::Value & options )
{
    switch ( options["model"].asInt() ) {
        case USER:
        {
            obj = new db_user( connection );
            return obj->Delete(bs);
        }
        case GOAL:
        {
            obj = new db_goal( connection );
            string param = options["params"].asString();
            Json::Value tmp = bs->ToJson();
            if( param == "DEFAULT" )
                tmp["userId"] = -1;

            BaseModel * goal = new Goal(tmp);
            goal->FromJson(tmp);
            return obj->Delete(goal);
        }
        case RECORD:
        {
            obj = new db_record( connection );
            return obj->Delete(bs);
        }
        case STRIKE:
        {
            obj = new db_report( connection );
            return obj->Delete(bs);
        }
        case MODERATOR:
        {
            obj = new db_moderator();
        }
        default:
            return -100;
    }
}

Json::Value DataBaseController::SelectOne(BaseModel * bs, const Json::Value & options )
{
    switch ( options["model"].asInt() )
    {
        case USER:
        {
            obj = new db_user( connection );
            Json::Value source = bs->ToJson();
            if( options["params"].asString() == "LOGIN" )
                source["id"] = -1;

            BaseModel * user = new User;
            user->FromJson(source);
            string str;
            Json::Value data =  obj->SelectOne(user);
            for (auto const& key : data.getMemberNames())
            {
                if( key != "status" && key != "empty" && key != "id" )
                {
                    str = data[key].asString();
                    str.erase( str.find_last_not_of(" \n\r\t")+1);
                    data[key]= str;
                }
            }
            return data;
        }
        case GOAL:
        {
            obj = new db_goal( connection );
            Json::Value resultJson = obj->SelectOne(bs);
            string str;
            for (auto const& key : resultJson.getMemberNames())
            {
                if( key != "id" && key != "status" && key != "empty" )
                {
                    str = resultJson[key].asString();
                    str.erase( str.find_last_not_of(" \n\r\t")+1);
                    resultJson[key]= str;
                }
            }
            return resultJson;
        }
        case RECORD:
        {
            obj = new db_record( connection );
            string str;
            string querySQL;
            if ( options["params"].asString() == "DEFAULT" )
                querySQL = "SELECT * FROM RECORDS WHERE id=$1;";
            else if ( options["params"].asString() == "GOALS" )
                querySQL = "SELECT g.id, g.title, g.description FROM RECORDS r JOIN GOALS g on g.id = r.idGoal WHERE r.id=$1;";
            else
                querySQL = "SELECT u.id, u.firstName, u.lastName, u.email, u.login, u.password"
                           " FROM RECORDS r JOIN USERS u on u.id = r.idUser WHERE r.id=$1;";

            connection->prepare("select_one", querySQL);
            Json::Value data = obj->SelectOne(bs);
            Json::Value resultJson;
            if( options["params"].asString() == "DEFAULT" )
            {
                resultJson["id"] = data["0"].asString();
                resultJson["description"] = data["1"].asString();
                resultJson["reportStatus"] =data["2"].asString();
                resultJson["idUser"] = data["3"].asString();
                resultJson["idGoal"] = data["4"].asString();
            }
            else if( options["params"].asString() == "GOALS" )
            {
                resultJson["id"] = data["0"].asString();
                resultJson["title"] = data["1"].asString();
                resultJson["description"] =data["2"].asString();
            }
            else if( options["params"].asString() == "USERS" )
            {
                resultJson["id"] = data["0"].asString();
                resultJson["firstName"] = data["1"].asString();
                resultJson["lastName"] =data["2"].asString();
                resultJson["email"] =data["3"].asString();
                resultJson["login"] =data["4"].asString();
                resultJson["password"] =data["5"].asString();
            }
            else
            {
                resultJson["status"] = false;
            }
            resultJson["status"] = data["status"].asBool();
            resultJson["empty"] = data["empty"].asBool();
            for (auto const& key : resultJson.getMemberNames())
            {
                if( key != "status" && key != "empty" && key != "id" &&
                    key != "idGoal" && key != "idUser" && key != "reportStatus" )
                {
                    str = resultJson[key].asString();
                    str.erase( str.find_last_not_of(" \n\r\t")+1);
                    resultJson[key]= str;
                }
                else if ( key == "id" || key == "idGoal" || key == "idUser")
                {
                    str = resultJson[key].asString();
                    resultJson[key] = std::stoi(str);
                }
                else if( key == "reportStatus" )
                {
                    resultJson[key] = data[key].asBool();
                }

            }
            return resultJson;
        }
    }
}

Json::Value DataBaseController::SelectMany(BaseModel * bs, const Json::Value & options )
{
    switch ( options["model"].asInt() )
    {
        case USER:
        {
            string fieldOut;
            Json::Value data;
            obj = new db_user( connection );
            string second_DB = options["params"].asString();
            if( second_DB == "DEFAULT" )
            {
                fieldOut = "firstName";
                connection->prepare("select_many",  "SELECT * FROM USERS where firstName != $1;");
                data = obj->SelectMany(bs);
            }
            else if ( second_DB == "GOALS")
            {
                fieldOut = "title";
                string res = "SELECT g.id, title FROM USERTOGOAL ug "
                             "JOIN GOALS g on g.id = ug.idGoal "
                             "JOIN USERS u on u.id = ug.idUser "
                             "WHERE u.id = $1 ORDER BY g.title;";
                connection->prepare("select_many",  res);
                data = obj->SelectMany(bs);
            }
            else if( second_DB == "RECORDS" )
            {
                fieldOut = "description";
                string res = "SELECT id, description FROM RECORDS"
                             " WHERE idUser = $1;";
                connection->prepare("select_many",  res);
                data = obj->SelectMany(bs);
            }
            else if( second_DB == "REPORTS" )
            {
                fieldOut = "description";
                string res = "SELECT id, description FROM REPORTS"
                             " WHERE fromId = $1;";
                connection->prepare("select_many",  res);
                data = obj->SelectMany(bs);
            }
            Json::Value resData;
            resData["status"] = data["status"];
            resData["empty"] = data["empty"];
            string str;
            for (auto const& id : data.getMemberNames())
            {
                if( id != "status" && id != "empty" )
                {
                    resData[id]["id"] = data[id]["first"].asString();
                    str  = data[id]["second"].asString();
                    str.erase( str.find_last_not_of(" \n\r\t")+1);
                    resData[id][fieldOut] = str;
                }
            }
            return resData;
        }
        case GOAL:
        {
            obj = new db_goal( connection );
            string param = options["params"].asString();
            string resQuery;
            string fieldOut;
            if( param == "RECORDS" )
            {
                resQuery = "SELECT r.id, r.description FROM records r "
                           "JOIN GOALS g on g.id = r.idGoal "
                           "WHERE g.id = $1;";
                fieldOut = "description";
            }
            else if( param == "USERS")
            {
                resQuery = "SELECT u.id, u.firstName "
                           "FROM USERTOGOAL ug "
                           "JOIN USERS u  on u.id = ug.idUser "
                           "JOIN GOALS g on g.id = ug.idGoal "
                           "WHERE g.id = $1 ORDER BY u.firstName;";
                fieldOut = "firstName";
            }
            else
            {
                resQuery = "SELECT * FROM GOALS where title != $1;";
                fieldOut = "title";
            }

            connection->prepare("select_many", resQuery);
            Json::Value data = obj->SelectMany(bs);
            Json::Value resData;
            resData["status"] = data["status"];
            resData["empty"] = data["empty"];
            string str;
            for (auto const& id : data.getMemberNames())
            {
                if( id != "status" && id != "empty" )
                {
                    resData[id]["id"] = data[id]["first"].asString();
                    str  = data[id]["second"].asString();
                    str.erase( str.find_last_not_of(" \n\r\t")+1);
                    resData[id][fieldOut] = str;
                }
            }
            return resData;
        }
        case RECORD:
        {
            obj = new db_record( connection );
            return obj->SelectMany( bs );
        }
    }
}

Json::Value DataBaseController::Update(BaseModel * bs, const Json::Value & options )
{
    switch ( options["model"].asInt() )
    {
        case USER:
        {
            obj = new db_user( connection );
            return obj->Update(bs);
        }
        case GOAL:
        {
            obj = new db_goal( connection );
            string param = options["params"].asString();
            Json::Value tmp = bs->ToJson();
            if( param == "DEFAULT" )
               tmp["userId"] = -1;

            BaseModel * goal = new Goal(tmp);
            goal->FromJson(tmp);
            return obj->Update(goal);
        }
        case RECORD:
        {
            obj = new db_record( connection );
            return obj->Update(bs);
        }
    }
}

DataBaseController::~DataBaseController()
{
    delete obj;
}