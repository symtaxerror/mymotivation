#include "db_record.h"

db_record::db_record( pqxx::connection * connection ):base_db( connection )
{
    connection->prepare("insert_to_record", "insert into RECORDS "
                                          "(description, reportstatus, iduser, idgoal)"
                                          " values ($1, $2, $3, $4);");
    connection->prepare("delete_record", "DELETE FROM RECORDS WHERE id=$1;");
    connection->prepare("select_record", "SELECT * FROM RECORDS WHERE id=$1;");
    connection->prepare("select_last_record", "SELECT * FROM RECORDS ORDER BY id desc LIMIT 1;");
    connection->prepare("update_record", "UPDATE RECORDS SET "
                                       "description =$1, reportStatus=$2, idUser=$3, idGoal=$4 "
                                       " WHERE id=$5;");
    connection->prepare("select_many_records", "SELECT * FROM RECORDS;");
//    connection->prepare("select_all_record", "SELECT * FROM USERS ;");
//    connection->prepare("update_user", "UPDATE USERS SET "
//                                       "firstName =$1, lastName=$2, email=$3, login=$4, password=$5 "
//                                       " WHERE id=$6;");

    work = new pqxx::work(*connection);
}

Json::Value db_record::Create(BaseModel * bs)
{
    Json::Value resultJson;
    pqxx::result res;
    Json::Value data = bs->ToJson();
    try
    {
        work->prepared("insert_to_record")
                (data["description"].asString())(data["reportStatus"].asBool())
                (data["idUser"].asInt())(data["idGoal"].asInt()).exec();
        work->commit();
        work = new pqxx::work(*connection);
        res = work->prepared("select_last_record").exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        resultJson["status"] = false;
        return resultJson;
    }
    resultJson["status"] = true;
    resultJson["id"] = atoi( res[0][0].c_str() );

    return resultJson;
}

Json::Value db_record::Delete(BaseModel * bs )
{
    Json::Value data = bs->ToJson();
    Json::Value res;
    try
    {
        work->prepared("delete_record")(data["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        res["status"] = false;
        return res;
    }
    res["id"] = data["id"].asInt();
    res["status"] = true;
    return res;
}

Json::Value db_record::Update( BaseModel * bs )
{
    Json::Value res;
    Json::Value data = bs->ToJson();
    try
    {
        work->prepared("update_record")(data["description"].asString())(data["reportStatus"].asBool())
                (data["idUser"].asInt())(data["idGoal"].asInt())
                (data["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        res["status"] = false;
        return res;
    }
    res["id"] = data["id"].asInt();
    res["status"] = true;
    return res;
}

Json::Value db_record::SelectOne(BaseModel * bs)
{
    Json::Value sourceJson = bs->ToJson();
    Json::Value resultJson;
    pqxx::result res;
    try
    {
        res = work->prepared("select_one")(sourceJson["id"].asInt()).exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        resultJson["status"] = false;
        return resultJson;
    }
    if( res.empty() )
    {
        resultJson["empty"] = true;
        resultJson["status"] = true;
        return resultJson;
    }

    int i = 0;
    for( auto line : res )
    {
        for( auto key : line )
        {
            resultJson[std::to_string(i++)] = key.c_str();
        }
    }
    resultJson["status"] = true;
    resultJson["empty"] = false;

    return resultJson;
}

Json::Value db_record::SelectMany(BaseModel * bs)
{
    Json::Value sourceJson = bs->ToJson();
    Json::Value resultJson;
    pqxx::result res;
    try
    {
        res = work->prepared("select_many_records").exec();
        work->commit();
    }
    catch (std::exception const &e)
    {
        std::cout << e.what();
        resultJson["status"] = false;
        return resultJson;
    }
    if( res.empty() )
    {
        resultJson["empty"] = true;
        resultJson["status"] = true;
        return resultJson;
    }
    resultJson["status"] = true;
    resultJson["empty"] = false;
    for( auto line : res )
    {
        resultJson[line[0].c_str()]["id"] = atoi(line[0].c_str());
        resultJson[line[0].c_str()]["description"] = line[1].c_str();
        resultJson[line[0].c_str()]["reportStatus"] = line[2].c_str() == "true";
        resultJson[line[0].c_str()]["idUser"] = atoi(line[3].c_str());
        resultJson[line[0].c_str()]["idGoal"] = atoi(line[4].c_str());
    }
    std::cout << resultJson;
    return resultJson;
}


