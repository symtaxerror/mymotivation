#ifndef MYBD_PROXY_DB_H
#define MYBD_PROXY_DB_H
#include "interface_db.h"
#include "data_base_controler.h"

//Кэширование и логирование
class proxy_db: public interface_db
{
public:
    proxy_db();
    Json::Value Create(BaseModel * bs, const Json::Value & options ) override;
    Json::Value SelectOne(BaseModel * bs, const Json::Value & options ) override;
    Json::Value SelectMany(BaseModel * bs, const Json::Value & options ) override;
    Json::Value Update(BaseModel * bs, const Json::Value & options ) override;
    Json::Value Delete(BaseModel * bs, const Json::Value & options ) override;
    ~proxy_db() override;

private:
    DataBaseController * service;
};
#endif //MYBD_PROXY_DB_H
