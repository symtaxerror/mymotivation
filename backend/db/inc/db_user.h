#ifndef MYBD_DB_USER_H
#define MYBD_DB_USER_H
#include "base_db.h"
#include "db_goal.h"
#include "db_record.h"
#include "db_report.h"

class db_user : public base_db
{
public:
    db_user( pqxx::connection * connection );
    Json::Value Create(BaseModel * bs) override;
    Json::Value Update( BaseModel * bs ) override;
    Json::Value Delete(BaseModel * bs) override;
    Json::Value SelectOne(BaseModel * bs) override;
    Json::Value SelectMany(BaseModel * bs) override;

private:
    pqxx::work * work;

    vector<shared_ptr<base_db>> records;
    vector<shared_ptr<base_db>> reports;
    vector<weak_ptr<base_db>> goals;
};


#endif //MYBD_DB_USER_H
