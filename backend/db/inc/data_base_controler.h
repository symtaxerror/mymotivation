#ifndef MYBD_DATA_BASE_CONTROLER_H
#define MYBD_DATA_BASE_CONTROLER_H
#include "interface_db.h"
#include "base_db.h"
#include "db_user.h"
#include "db_goal.h"
#include "db_record.h"
#include "db_report.h"
#include "db_moderator.h"
#include "db_session.h"
#include <jsoncpp/json/json.h>
#include "../../models/inc/base_model.hpp"
#include "../../models/inc/goal.hpp"
#include "string"
class DataBaseController: public interface_db
{
public:
    DataBaseController();
    DataBaseController(base_db * obj);
    Json::Value Create(BaseModel * bs, const Json::Value & options ) override;
    Json::Value SelectOne(BaseModel * bs, const Json::Value & options ) override;
    Json::Value SelectMany(BaseModel * bs, const Json::Value & options ) override;
    Json::Value Update(BaseModel * bs, const Json::Value & options ) override;
    Json::Value Delete(BaseModel * bs, const Json::Value & options ) override;
    ~DataBaseController() override;
private:
    base_db * obj;
    pqxx::connection * connection;
};
#endif //MYBD_DATA_BASE_CONTROLER_H
