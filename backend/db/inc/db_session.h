#ifndef MYBD_DB_SESSION_H
#define MYBD_DB_SESSION_H
#include "base_db.h"
#include "db_record.h"

class db_session : public base_db
{
public:
    Json::Value Create(BaseModel * bs) override;
    Json::Value Update( BaseModel * bs ) override;
    Json::Value Delete(BaseModel * bs) override;
    Json::Value SelectOne(BaseModel * bs) override;
    Json::Value SelectMany(BaseModel * bs) override;

private:
    weak_ptr<base_db> record;
};

#endif //MYBD_DB_SESSION_H