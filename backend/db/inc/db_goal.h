#ifndef MYBD_DB_GOAL_H
#define MYBD_DB_GOAL_H
#include "base_db.h"

class db_goal : public base_db
{
public:
    db_goal( pqxx::connection * connection );
    Json::Value Create(BaseModel * bs) override;
    Json::Value Update( BaseModel * bs ) override;
    Json::Value Delete( BaseModel * bs ) override;
    Json::Value SelectOne(BaseModel * bs) override;
    Json::Value SelectMany(BaseModel * bs) override;

private:
    pqxx::work * work;

    vector<shared_ptr<base_db>> users;
    vector<shared_ptr<base_db>> reports;
};
#endif //MYBD_DB_GOAL_H
