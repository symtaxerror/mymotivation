#ifndef MYBD_BASE_DB_H
#define MYBD_BASE_DB_H
#include <map>
#include <vector>
#include <memory>
#include <pqxx/pqxx>
#include <iostream>
#include <jsoncpp/json/json.h>
#include "../../models/inc/user.hpp"

using std::string;
using std::vector;
using std::shared_ptr;
using std::weak_ptr;

class base_db
{
public:
    base_db( pqxx::connection * connection );
    base_db();
    virtual Json::Value Create( BaseModel * bs );
    virtual Json::Value Update(BaseModel * bs );
    virtual Json::Value Delete( BaseModel * bs);
    virtual Json::Value SelectOne(BaseModel * bs);
    virtual Json::Value SelectMany(BaseModel * bs);
    virtual ~base_db() = default;

protected:
    pqxx::connection * connection;
};

#endif //MYBD_BASE_DB_H
