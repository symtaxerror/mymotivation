#ifndef MYBD_DB_MODERATOR_H
#define MYBD_DB_MODERATOR_H
#include "base_db.h"
#include "db_report.h"

class db_moderator : public base_db
{
public:
    Json::Value Create(BaseModel * bs) override;
    Json::Value Update( BaseModel * bs ) override;
    Json::Value Delete( BaseModel * bs ) override;
    Json::Value SelectOne(BaseModel * bs) override;
    Json::Value SelectMany(BaseModel * bs) override;


private:
    vector<weak_ptr<base_db>> reports;
};
#endif //MYBD_DB_MODERATOR_H
