#ifndef MYBD_DB_REPORT_H
#define MYBD_DB_REPORT_H
#include "base_db.h"
#include "db_record.h"

class db_report : public base_db
{
public:
    db_report( pqxx::connection * connection );
    Json::Value Create(BaseModel * bs) override;
    Json::Value Update( BaseModel * bs ) override;
    Json::Value Delete(BaseModel * bs) override;
    Json::Value SelectOne(BaseModel * bs) override;
    Json::Value SelectMany(BaseModel * bs) override;

private:
    pqxx::work * work;
    weak_ptr<base_db> record;
};

#endif //MYBD_DB_REPORT_H
