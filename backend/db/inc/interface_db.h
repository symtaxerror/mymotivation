//
// Created by sap on 09.11.2020.
//

#ifndef MYBD_INTERFACE_DB_H
#define MYBD_INTERFACE_DB_H
#include "../../models/inc/base_model.hpp"
#include <map>
#include <vector>
using std::map;
using std::string;
using std::vector;

class interface_db
{
public:
    interface_db();
    virtual Json::Value Create(BaseModel * bs, const Json::Value & options );
    virtual Json::Value SelectOne(BaseModel * bs, const Json::Value & options );
    virtual Json::Value SelectMany(BaseModel * bs, const Json::Value & options );
    virtual Json::Value Update(BaseModel * bs, const Json::Value & options );
    virtual Json::Value Delete(BaseModel * bs, const Json::Value & options );
    virtual ~interface_db();
};

int initDB();

#endif //MYBD_INTERFACE_DB_H

