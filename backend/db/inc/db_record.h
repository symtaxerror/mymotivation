#ifndef MYBD_DB_RECORD_H
#define MYBD_DB_RECORD_H
#include "base_db.h"
#include "db_user.h"
#include "db_goal.h"

class db_record : public base_db
{
public:
    db_record( pqxx::connection * connection );
    Json::Value Create(BaseModel * bs) override;
    Json::Value Update( BaseModel * bs ) override;
    Json::Value Delete(BaseModel * bs ) override;
    Json::Value SelectOne(BaseModel * bs) override;
    Json::Value SelectMany(BaseModel * bs) override;

private:
    pqxx::work * work;

    weak_ptr<base_db> user;
    weak_ptr<base_db> goal;
};
#endif //MYBD_DB_RECORD_H
