#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "inc/proxy_db.h"
#include "inc/interface_db.h"
//#include <postgresql/libpq-fe.h>
#include <pqxx/pqxx>
//#include "../../models/src/goal.cpp"
//#include "../../models/src/record.cpp"
//#include "../../models/src/user.cpp"
#include "user.hpp"
#include "record.hpp"
#include "goal.hpp"
#include "strike.hpp"
//#include "../../models/inc/base_model.hpp"
#include "init_db_table.cpp"
#include <map>
#include <vector>
#include <jsoncpp/json/json.h>
using std::map;
using std::string;
using std::vector;
using namespace std;

int main()
{
//    initDB();
//    Json::Value goal;
//    goal["id"] = 1;
//    goal["firstName"] = "s";
//    goal["lastName"] ="sd";
//    goal["email"] = "sdf";
//    Json::Value strike;
//    strike["id"] = 2;
//    strike["fromId"] = 3;
//    strike["recordId"] = 8;
//    strike["status"] = "st";
//    strike["description"] = "sdfsf";
    Json::Value user;
    user["id"] = 3;
    user["firstName"] = "sdsdfsdfs";
    user["lastName"] = "sdfsd";
    user["email"] = "sd";
    user["login"] = "df";
    user["password"] = "sfdsd";

    Json::Value options;
    options["model"] = USER;
    options["params"] = "REPORTS"; // GOALS, REPORTS
    BaseModel * bs = new User(user);
    bs->FromJson(user);

    interface_db * proxy = new proxy_db();
    Json::Value res = proxy->SelectMany(bs, options );

    cout << res;

    delete proxy;

}
