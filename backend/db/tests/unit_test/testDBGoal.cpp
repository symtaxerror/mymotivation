#include "gtest/gtest.h"

extern "C" {
#include "db_goal.h"
#include <cstdlib>
}
using std::string;
using std::map;


TEST (DBGoal, createAndSelectOne)
{
    int id = 1;
    string name = "Ivan";
    int CostToEnter = 1000;
    int Balance = 2000;
    string Description = "цель";
    int dateStart = 1201390;
    int dateEnd = 1201390;

    map <string, string> :: iterator it, it_2;
    DBGoal * goal = new DBGoal();
    goal->setParameters(id, name, CostToEnter, Balance, Description, dateStart, dateEnd);
    goal->Create();
    delete goal;

    DBGoal * savedGoal = new DBGoal();
    savedGoal->setId(id);
    map<string, string> dataFromDb = savedGoal->SelectOne();
    delete savedGoal;

    ASSERT_NE (dataFromDb.end(),dataFromDb.find("id"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("name"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("CostToEnter"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("Balance"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("Description"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("dateStart"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("dateEnd"));

    EXPECT_EQ (id, atoi(dataFromDb.find("id")->second.c_str()));
    EXPECT_EQ (name, dataFromDb.find("name")->second);
    EXPECT_EQ (CostToEnter, atoi(dataFromDb.find("CostToEnter")->second.c_str()));
    EXPECT_EQ (Balance, atoi(dataFromDb.find("Balance")->second.c_str()));
    EXPECT_EQ (Description, dataFromDb.find("Description")->second);
    EXPECT_EQ (dateStart, atoi(dataFromDb.find("dateStart")->second.c_str()));
    EXPECT_EQ (dateEnd, atoi(dataFromDb.find("dateEnd")->second.c_str()));
}

TEST (DBGoal, del)
{
    int id = 1;
    string name = "Ivan";
    int CostToEnter = 1000;
    int Balance = 2000;
    string Description = "цель";
    int dateStart = 1201390;
    int dateEnd = 1201390;

    DBGoal * goal = new DBGoal();
    goal->setParameters(id, name, CostToEnter, Balance, Description, dateStart, dateEnd);
    goal->Create();
    delete goal;

    DBGoal * goalDel = new DBGoal();
    goalDel->setId(id);
    goalDel->Delete();
    delete goalDel;

    DBGoal * goalSelect = new DBGoal();
    goalSelect->setId(id);
    map<string, string> dataFromDb = goalSelect->SelectOne();
    delete goalSelect;

    ASSERT_NE (dataFromDb.end(),dataFromDb.find("status"));
    EXPECT_EQ ("err", dataFromDb.find("status")->second);
}

TEST (DBGoal, update)
{
    int id = 1;
    string name = "Ivan";
    int CostToEnter = 1000;
    int Balance = 2000;
    string Description = "цель";
    int dateStart = 1201390;
    int dateEnd = 1201390;

    DBGoal * goal = new DBGoal();
    goal->setParameters(id, name, CostToEnter, Balance, Description, dateStart, dateEnd);
    goal->Create();
    delete goal;

    DBGoal * savedGoal = new DBGoal();
    Balance = 100;
    savedGoal->setParameters(id, name, CostToEnter, Balance, Description, dateStart, dateEnd);
    savedGoal->Update();
    delete savedGoal;

    DBGoal * updatedGoal = new DBGoal();
    updatedGoal->setId(id);
    map<string, string> dataFromDb = updatedGoal->SelectOne();
    delete updatedGoal;

    ASSERT_NE (dataFromDb.end(),dataFromDb.find("id"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("name"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("CostToEnter"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("Balance"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("Description"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("dateStart"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("dateEnd"));

    EXPECT_EQ (id, atoi(dataFromDb.find("id")->second.c_str()));
    EXPECT_EQ (name, dataFromDb.find("name")->second);
    EXPECT_EQ (CostToEnter, atoi(dataFromDb.find("CostToEnter")->second.c_str()));
    EXPECT_EQ (Balance, atoi(dataFromDb.find("Balance")->second.c_str()));
    EXPECT_EQ (Description, dataFromDb.find("Description")->second);
    EXPECT_EQ (dateStart, atoi(dataFromDb.find("dateStart")->second.c_str()));
    EXPECT_EQ (dateEnd, atoi(dataFromDb.find("dateEnd")->second.c_str()));
}
