#include "gtest/gtest.h"

extern "C" {
#include "data_base_controler.h"
#include "db_user.h"
}

using std::string;
using std::map;

TEST (DBController, createAndSelect)
{
    map<string, string> userData = {{"id", "1"},
                                      {"first", "Ivan"},
                                      {"lastName", "Ivanov"},
                                      {"mail", "@mail.ru"},
                                      {"password", " asdasa"},
                                      {"balance", "100"}};

    WrapperDb wrapp;
    wrapp.setWrap(USER, userData);
    DataBaseController controller;
    controller.Create(wrapp);

    map<string, string> userDataDB = controller.SelectOne(wrapp);

    ASSERT_NE (userDataDB.end(),userDataDB.find("status"));
    EXPECT_EQ (userData, userDataDB);
}

TEST (DBController, update) {
    map<string, string> userData = {{"id",       "1"},
                                    {"first",    "Ivan"},
                                    {"lastName", "Ivanov"},
                                    {"mail",     "@mail.ru"},
                                    {"password", " asdasa"},
                                    {"balance",  "100"}};

    WrapperDb wrapp;
    wrapp.setWrap(USER, userData);
    DataBaseController controller;
    controller.Create(wrapp);

    map<string, string> updateUserData = {{"id",       "1"},
                                          {"first",    "Fedor"},
                                          {"lastName", "Fedorov"},
                                          {"mail",     "@mail.ru"},
                                          {"password", " asdasa"},
                                          {"balance",  "100"}};

    wrapp.setWrap(USER, updateUserData);
    controller.Update(wrapp);

    map<string, string> userDataDB = controller.SelectOne(wrapp);

    ASSERT_NE (userDataDB.end(),userDataDB.find("status"));
    EXPECT_EQ (updateUserData, userDataDB);
}

TEST (DBController, selectMany) {
    map<string, string> userData = {{"id",       "1"},
                                    {"first",    "Ivan"},
                                    {"lastName", "Ivanov"},
                                    {"mail",     "@mail.ru"},
                                    {"password", " asdasa"},
                                    {"balance",  "100"}};

    WrapperDb wrapp;
    wrapp.setWrap(USER, userData);
    DataBaseController controller;
    controller.Create(wrapp);

    map<string, string> newUserData = {{"id",       "2"},
                                          {"first",    "Fedor"},
                                          {"lastName", "Fedorov"},
                                          {"mail",     "@mail.ru"},
                                          {"password", " asdasa"},
                                          {"balance",  "100"}};

    wrapp.setWrap(USER, newUserData);
    controller.Create(wrapp);

    vector<map<string, string>> userDataDB = controller.SelectMany(wrapp);

    ASSERT_EQ (2,userDataDB.size());

    ASSERT_NE (userDataDB[0].end(),userDataDB[0].find("status"));
    ASSERT_NE (userDataDB[0].end(),userDataDB[1].find("status"));

    EXPECT_EQ (userData, userDataDB[0]);
    EXPECT_EQ (newUserData, userDataDB[1]);
}

TEST (DBController, del) {
    map<string, string> userData = {{"id",       "1"},
                                    {"first",    "Ivan"},
                                    {"lastName", "Ivanov"},
                                    {"mail",     "@mail.ru"},
                                    {"password", " asdasa"},
                                    {"balance",  "100"}};

    WrapperDb wrapp;
    wrapp.setWrap(USER, userData);
    DataBaseController controller;
    controller.Create(wrapp);

    controller.Delete(wrapp);

    map<string, string> userDataDB = controller.SelectOne(wrapp);

    ASSERT_NE (userDataDB.end(),userDataDB.find("status"));
    EXPECT_EQ ("err", userDataDB.find("status")->second);
}
