#include "gtest/gtest.h"

extern "C" {
#include "db_user.h"
#include <cstdlib>
}

using std::string;
using std::map;

TEST (DBUser, createAndSelectOne)
{
    int id = 1;
    string firstName = "Ivan";
    string lastName = "Ivanov";
    string mail = "@mail.ru";
    string password = "asda123fwfk32";
    int balance = 100;

    DBUser * user = new DBUser();
    user->setParameters(id, firstName, lastName, mail, password, balance);
    user->Create();
    delete user;

    DBUser * savedUser = new DBUser();
    user->setId(id);
    map<string, string> dataFromDb = savedUser->SelectOne();
    delete savedUser;

    ASSERT_NE(dataFromDb.end(),dataFromDb.find("id"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("firstName"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("lastName"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("mail"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("password"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("balance"));

    EXPECT_EQ (id, atoi(dataFromDb.find("id")->second.c_str()));
    EXPECT_EQ (firstName, dataFromDb.find("firstName")->second);
    EXPECT_EQ (lastName, dataFromDb.find("lastName")->second);
    EXPECT_EQ (mail, dataFromDb.find("mail")->second);
    EXPECT_EQ (password, dataFromDb.find("password")->second);
    EXPECT_EQ (balance, atoi(dataFromDb.find("balance")->second.c_str()));
}

TEST (DBUser, del)
{
    int id = 1;
    string firstName = "Ivan";
    string lastName = "Ivanov";
    string mail = "@mail.ru";
    string password = "asda123fwfk32";
    int balance = 100;

    map <string, string> :: iterator it, it_2;
    DBUser * user = new DBUser();
    user->setParameters(id, firstName, lastName, mail, password, balance);
    user->Create();
    delete user;

    DBUser * userDel = new DBUser();
    userDel->setId(id);
    userDel->Delete();
    delete userDel;

    DBUser * deletedUser = new DBUser();
    deletedUser->setId(id);
    map<string, string> dataFromDb = deletedUser->SelectOne();
    delete deletedUser;

    ASSERT_NE (dataFromDb.end(),dataFromDb.find("status"));
    EXPECT_EQ ("err", dataFromDb.find("status")->second);
}

TEST (DBUser, update)
{
    int id = 1;
    string firstName = "Ivan";
    string lastName = "Ivanov";
    string mail = "@mail.ru";
    string password = "asda123fwfk32";
    int balance = 100;

    DBUser * user = new DBUser();
    user->setParameters(id, firstName, lastName, mail, password, balance);
    user->Create();
    delete user;

    DBUser * savedUser = new DBUser();
    balance = 200;
    savedUser->setParameters(id, firstName, lastName, mail, password, balance);
    savedUser->Update();
    delete savedUser;

    DBUser * updatedUser = new DBUser();
    updatedUser->setId(id);
    map<string, string> dataFromDb = updatedUser->SelectOne();
    delete updatedUser;

    ASSERT_NE(dataFromDb.end(),dataFromDb.find("id"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("firstName"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("lastName"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("mail"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("password"));
    ASSERT_NE (dataFromDb.end(),dataFromDb.find("balance"));

    EXPECT_EQ (id, atoi(dataFromDb.find("id")->second.c_str()));
    EXPECT_EQ (firstName, dataFromDb.find("firstName")->second);
    EXPECT_EQ (lastName, dataFromDb.find("lastName")->second);
    EXPECT_EQ (mail, dataFromDb.find("mail")->second);
    EXPECT_EQ (password, dataFromDb.find("password")->second);
    EXPECT_EQ (balance, atoi(dataFromDb.find("balance")->second.c_str()));
}

