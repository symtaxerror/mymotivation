cmake_minimum_required(VERSION 3.1)

project(app)

set(COMPILE_FLAGS "-std=c++17 -ljsoncpp -lpqxx -lpq -lcrypto -ljwt -lssl -lpthread -lfcgi")
add_definitions(${COMPILE_FLAGS})

include_directories("${PROJECT_SOURCE_DIR}/api/inc")
include_directories("${PROJECT_SOURCE_DIR}/auth/inc")
include_directories("${PROJECT_SOURCE_DIR}/controllers/inc")
include_directories("${PROJECT_SOURCE_DIR}/db/inc")
include_directories("${PROJECT_SOURCE_DIR}/models/inc")
include_directories("${PROJECT_SOURCE_DIR}/routing/inc")

file(GLOB sources "${PROJECT_SOURCE_DIR}/api/src/*.c" "${PROJECT_SOURCE_DIR}/auth/src/*.c" "${PROJECT_SOURCE_DIR}/routing/src/*.c"
"${PROJECT_SOURCE_DIR}/controllers/src/*.c" "${PROJECT_SOURCE_DIR}/db/src/*.c" "${PROJECT_SOURCE_DIR}/models/src/*.c")

add_executable(app ${sources} mainloop.cpp)
link_libraries(app libpqxx)