#include "jwt_scheme.hpp"

#include <unistd.h>

#include <time.h>

#include <iostream>

#include "config.hpp"
#include "jwt/jwt_all.h"

using json = nlohmann::json;
using std::cout;
using std::endl;

bool authenticate(HTTP::Request &request) {
    string token = request.headers["AUTHORIZATION"];

    HS256Validator signer(SECRET_KEY);
    ExpValidator exp;
    try {
        ::json header, payload;

        std::tie(header, payload) = JWT::Decode(token, &signer, &exp);
        request.isAuthenticated = true;
        std::cout << payload;
        request.user = payload["id"];
        return true;
    } catch (InvalidTokenError) {
        return false;
    }
}

string generateToken(Json::Value data) {
    HS256Validator signer(SECRET_KEY);
    data["exp"] = (int)(time(nullptr) + TIME_TO_EXPIRE);
    auto payload = json::parse(data.toStyledString());
    return JWT::Encode(signer, payload);
}
