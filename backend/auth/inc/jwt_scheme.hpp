#pragma once

#include "api.hpp"

bool authenticate(HTTP::Request &request);

string generateToken(Json::Value payload);
